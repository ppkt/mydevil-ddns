import collections
import json
import socket
from ipaddress import IPv4Address, IPv6Address
from typing import Union, List, Dict

from flask import Flask

# path to devil socket
SOCKET_ADDRESS = '/var/run/devil2.sock'

# TTL for new records
TTL = 60

DevilDnsEntry = collections.namedtuple(
    'DevilDnsRecord', 'id record dns_type prio weight ttl content domain')


class DevilCommandError(Exception):
    """Raised when devil returns unexpected response"""
    pass


class DevilClient:
    """Client to interact with Devil by unix socket"""

    def __init__(self, app=None):
        self.app = None
        self.logger = None

        if app is not None:
            self.init_app(app)

    def init_app(self, app):
        self.app: Flask = app
        self.logger = self.app.logger.getChild('devil')
        self.logger.debug('New APP attached')

    def _send_via_socket(self, data: bytes) -> bytes:
        """Send data to Devil using unix socket"""
        self.socket = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
        self.socket.connect(SOCKET_ADDRESS)
        self.socket.sendall(data)
        # TODO: Add support for reading data in chunks
        data = self.socket.recv(2 ** 14)
        self.socket.close()
        return data

    def _send_devil_cmd(self, *args) -> Dict:
        """Send command to Devil"""
        devil_arguments = ('--json', *args)

        # send command
        data = self._send_via_socket(json.dumps(devil_arguments).encode())

        # load and check response
        response = json.loads(data)

        if response['code'] == "OK":
            return response

        # report any errors
        self.logger.error(response)
        raise DevilCommandError(response)

    def get_hosts(self, domain: str) -> List:
        """Fetch list of DNS records for domain"""
        response = self._send_devil_cmd('dns', 'list', domain)
        return response['records']

    def add_or_update_dns_entry(
            self, domain: str, host: str,
            ip_address: Union[IPv4Address, IPv6Address]):
        """
        Add or update DNS record for `host` with ip `ip_address` in domain
        `domain`.
        If there is an existing host with the same IP address, do nothing.
        If there is an existing host with different IP address, delete it and
        replace by provided value.
        Return `true` if IP address was changed, `false` otherwise.
        """

        # determine record type
        if ip_address.version == 4:
            dns_type = 'A'
        else:
            dns_type = 'AAAA'

        new_hostname = f'{host}.{domain}'
        new_ip_address = ip_address.compressed

        # fetch list of existing records
        try:
            domains = self.get_hosts(domain)
        except Exception as e:
            self.logger.exception(e)
            raise

        for d in domains:
            entry = DevilDnsEntry(**d)
            if entry.record == new_hostname and entry.dns_type == dns_type:
                # do not update DNS table if IP hasn't changed
                if entry.content == new_ip_address:
                    self.logger.debug('No change in IP for: %s', entry)
                    return False

                # delete existing entry
                self._send_devil_cmd('dns', 'del', domain, str(entry.id))

        # add new host
        self._send_devil_cmd('dns', 'add', domain, new_hostname, dns_type,
                             new_ip_address, str(TTL))
        return True
