import logging
import os
import sys
from logging import FileHandler

from ddns import app as application, devil

devil.init_app(app=application)

handler = FileHandler(os.path.join(os.path.dirname(__file__), 'ddns.log'))
handler.setLevel(logging.DEBUG)
application.logger.addHandler(handler)

application.logger.setLevel(logging.DEBUG)
application.logger.debug('App up and ready')

sys.path.append(os.getcwd())
