import logging
import os
import sys
from ipaddress import IPv4Address, IPv6Address, AddressValueError, ip_address
from logging import FileHandler
from typing import Optional, Tuple

from flask import Flask, request, abort, json

from devil_client import DevilClient

app: Flask = Flask(__name__)
devil = DevilClient()

try:
    # load app settings from file 'settings.py'
    app.config.from_pyfile('settings.py')
except FileNotFoundError:
    app.logger.error('Settings file missing!')
    sys.exit(1)


@app.route('/')
def index():
    return ''


@app.route('/update/', methods=['POST'])
def update_dns_record():
    check_request_fields()

    host, ipv4_address, ipv6_address = validate_request_fields()
    ipv4_updated = ipv6_updated = None

    if ipv4_address:
        ipv4_updated = devil.add_or_update_dns_entry(
            domain=app.config['DDNS_DOMAIN'],
            host=host,
            ip_address=ipv4_address,
        )

    if ipv6_address:
        ipv6_updated = devil.add_or_update_dns_entry(
            domain=app.config['DDNS_DOMAIN'],
            host=host,
            ip_address=ipv6_address,
        )

    return json.dumps({'ipv4': ipv4_updated, 'ipv6': ipv6_updated})


def validate_request_fields() -> Tuple[str,
                                       Optional[IPv4Address],
                                       Optional[IPv6Address]]:
    """
    Ensure that all fields are valid and correct,
    """

    ipv4_addr = ipv6_addr = None
    host = request.form['host']

    # check if api key is valid
    if request.form['api_key'] != app.config['DDNS_API_KEY']:
        abort(401)

    # check if new domain is on approved list
    if host not in app.config['DDNS_MANAGED_HOSTS']:
        abort(400)

    # check if ipv4 address is correct (if provided)
    if 'ipv4' in request.form:
        try:
            ipv4_addr = IPv4Address(request.form['ipv4'])
        except AddressValueError:
            abort(400)

    # check if ipv6 address is correct (if provided)
    if 'ipv6' in request.form:
        try:
            ipv6_addr = IPv6Address(request.form['ipv6'])
        except AddressValueError:
            abort(400)

    if 'ip' in request.form:
        try:
            ip_addr = ip_address(request.form['ip'])
        except ValueError:
            return abort(400)

        if ip_addr.version == 4:
            ipv4_addr = ip_addr
        else:
            ipv6_addr = ip_addr

    # check if provided addresses are allowed
    for addr in [ipv4_addr, ipv6_addr]:
        if addr and (addr.is_loopback or addr.is_multicast or
                     addr.is_reserved or addr.is_link_local or
                     addr.is_unspecified):
            app.logger.error('IP Address is invalid: %s', addr)
            abort(400)

    return host, ipv4_addr, ipv6_addr


def check_request_fields():
    """
    Ensure that request has all required fields
    """

    # check if api and host fields are sent and either ipv4 or ipv6 (or both)
    fields_required = {'api_key', 'host'}
    typed_ip_addresses = {'ipv4', 'ipv6'}
    typeless_ip_addresses = {'ip'}
    form_keys = set(request.form.keys())

    # check if fields_required is subset of form_keys
    if fields_required < form_keys:
        app.logger.debug('all mandatory fields are in')
    else:
        abort(400)

    # create intersection with common items in both sets
    # dynamic ip address detection is mutually exclusive with providing values
    # explicitly
    typed = typed_ip_addresses & form_keys
    typeless = typeless_ip_addresses & form_keys
    if (typed or typeless) and not (typed and typeless):
        app.logger.debug('additional fields found: %s', typed)
    else:
        abort(400)


if __name__ == '__main__':
    devil.init_app(app)

    handler = FileHandler(os.path.join(os.path.dirname(__file__), 'ddns.log'))
    handler.setLevel(logging.DEBUG)
    app.logger.addHandler(handler)

    app.logger.setLevel(logging.DEBUG)
    app.logger.debug('App up and ready')
    app.run()
