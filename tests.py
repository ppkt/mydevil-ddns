import json
import unittest
from unittest.mock import Mock, patch, call

from flask.testing import FlaskClient

import ddns
from devil_client import DevilClient


class DdnsTestCase(unittest.TestCase):

    def setUp(self):
        ddns.app.testing = True
        self.app: FlaskClient = ddns.app.test_client()

        self.valid_request = {
            'api_key': '666',
            'host': 'trurl',
            'ipv4': '1.2.3.4',
            'ipv6': '2001:db8:85a3::8a2e:370:7334',
        }
        self.alt_ipv4_address = '4.3.2.1'
        self.alt_ipv6_address = '2001:db8:85a3::8a2e:370:7335'

        self.valid_request_ipv4 = {
            'api_key': self.valid_request['api_key'],
            'host': self.valid_request['host'],
            'ip': self.valid_request['ipv4'],
        }

        self.valid_request_ipv6 = {
            'api_key': self.valid_request['api_key'],
            'host': self.valid_request['host'],
            'ip': self.valid_request['ipv6'],
        }

        ddns.app.config['DDNS_API_KEY'] = self.valid_request['api_key']
        ddns.app.config['DDNS_MANAGED_HOSTS'] = [self.valid_request['host'], ]
        ddns.app.config['DDNS_DOMAIN'] = 'example.com'

        self.devil = ddns.devil = DevilClient(app=ddns.app)
        self.devil.get_hosts = Mock(return_value=[], )
        self.devil._send_via_socket = Mock(return_value=b'{"code": "OK"}')

    def test_index_does_return_nothing(self):
        r = self.app.get('/')
        self.assertEqual(r.status_code, 200)
        self.assertEqual(r.data, b'')

    def test_index_accept_only_get(self):
        r = self.app.post('/')
        self.assertEqual(r.status_code, 405)  # method not allowed

    def test_update_endpoint_exists(self):
        r = self.app.get('/update/')
        self.assertNotEqual(r.status_code, 404)

    def test_update_endpoint_does_not_accept_get(self):
        r = self.app.get('/update/')
        self.assertEqual(r.status_code, 405)

    def test_update_endpoint_returns_400_on_empty_pos(self):
        r = self.app.post('/update/')
        self.assertEqual(r.status_code, 400)

    def test_update_mandatory_arguments(self):
        r = self.app.post('/update/', data=self.valid_request)
        self.assertEqual(r.status_code, 200)

    def test_update_only_ipv4_is_success(self):
        del self.valid_request['ipv6']
        r = self.app.post('/update/', data=self.valid_request)
        self.assertEqual(r.status_code, 200)

    def test_update_only_ipv6_is_success(self):
        del self.valid_request['ipv4']
        r = self.app.post('/update/', data=self.valid_request)
        self.assertEqual(r.status_code, 200)

    def test_error_on_invalid_api_key(self):
        invalid_request = dict(self.valid_request,
                               api_key='totally_invalid_key')
        r = self.app.post('/update/', data=invalid_request)
        self.assertEqual(r.status_code, 401)

    def test_error_on_invalid_host(self):
        invalid_request = dict(self.valid_request, host='totally_invalid_host')
        r = self.app.post('/update/', data=invalid_request)
        self.assertEqual(r.status_code, 400)

    def test_error_on_invalid_ipv4_address(self):
        invalid_request = dict(self.valid_request, ipv4='256.255.255.255')
        r = self.app.post('/update/', data=invalid_request)
        self.assertEqual(r.status_code, 400)

    def test_error_on_invalid_ipv6_address(self):
        invalid_request = dict(self.valid_request,
                               ipv6='1200::AB00:1234::2552:7777:1313')
        r = self.app.post('/update/', data=invalid_request)
        self.assertEqual(r.status_code, 400)

    def test_add_new_ipv4_address(self):
        del self.valid_request['ipv6']
        r = self.app.post('/update/', data=self.valid_request)
        self.assertEqual(r.status_code, 200)
        self.assertEqual(r.data, b'{"ipv4": true, "ipv6": null}')

        expected_query = json.dumps([
            '--json', 'dns', 'add', 'example.com', 'trurl.example.com', 'A',
            self.valid_request['ipv4'], '60']).encode()

        self.devil._send_via_socket.assert_called_once_with(expected_query)

    def test_add_new_ipv6_address(self):
        del self.valid_request['ipv4']
        r = self.app.post('/update/', data=self.valid_request)
        self.assertEqual(r.status_code, 200)
        self.assertEqual(r.data, b'{"ipv4": null, "ipv6": true}')

        expected_query = json.dumps([
            '--json', 'dns', 'add', 'example.com', 'trurl.example.com', 'AAAA',
            self.valid_request['ipv6'], '60']).encode()

        self.devil._send_via_socket.assert_called_once_with(expected_query)

    def test_dont_update_domain_when_address_has_not_changed_ipv4(self):
        del self.valid_request['ipv6']

        self.devil.get_hosts = Mock(return_value=[{
            "id": 1,
            "record": "trurl.example.com",
            "dns_type": "A",
            "prio": "None",
            "weight": "None",
            "ttl": 60,
            "content": self.valid_request['ipv4'],
            "domain": 1,
        }], )

        # subsequent call without any changes
        r = self.app.post('/update/', data=self.valid_request)
        self.assertEqual(200, r.status_code)
        self.assertEqual(b'{"ipv4": false, "ipv6": null}', r.data)

        self.devil.get_hosts.assert_called_once_with('example.com')
        self.devil._send_via_socket.assert_not_called()

    def test_dont_update_domain_when_address_has_not_changed_ipv6(self):
        del self.valid_request['ipv4']

        self.devil.get_hosts = Mock(return_value=[{
            "id": 1,
            "record": "trurl.example.com",
            "dns_type": "AAAA",
            "prio": "None",
            "weight": "None",
            "ttl": 60,
            "content": self.valid_request['ipv6'],
            "domain": 1,
        }], )

        # subsequent call without any changes
        r = self.app.post('/update/', data=self.valid_request)
        self.assertEqual(200, r.status_code)
        self.assertEqual(b'{"ipv4": null, "ipv6": false}', r.data)

        self.devil.get_hosts.assert_called_once_with('example.com')
        self.devil._send_via_socket.assert_not_called()

    def test_update_ip_address_when_changed_ipv4(self):
        del self.valid_request['ipv6']

        self.devil.get_hosts = Mock(return_value=[{
            "id": 1,
            "record": "trurl.example.com",
            "dns_type": "A",
            "prio": "None",
            "weight": "None",
            "ttl": 60,
            "content": self.valid_request['ipv4'],
            "domain": 1,
        }], )

        self.valid_request['ipv4'] = self.alt_ipv4_address
        r = self.app.post('/update/', data=self.valid_request)
        self.assertEqual(200, r.status_code)
        self.assertEqual(b'{"ipv4": true, "ipv6": null}', r.data)

        expected_query_1 = json.dumps([
            '--json', 'dns', 'del', 'example.com', '1']).encode()
        expected_query_2 = json.dumps([
            '--json', 'dns', 'add', 'example.com', 'trurl.example.com', 'A',
            self.alt_ipv4_address, '60']).encode()

        self.assertEqual([call(expected_query_1), call(expected_query_2)],
                         self.devil._send_via_socket.call_args_list)

    def test_update_ip_address_when_changed_ipv6(self):
        del self.valid_request['ipv4']

        self.devil.get_hosts = Mock(return_value=[{
            "id": 1,
            "record": "trurl.example.com",
            "dns_type": "AAAA",
            "prio": "None",
            "weight": "None",
            "ttl": 60,
            "content": self.valid_request['ipv6'],
            "domain": 1,
        }], )

        self.valid_request['ipv6'] = self.alt_ipv6_address
        r = self.app.post('/update/', data=self.valid_request)
        self.assertEqual(r.status_code, 200)
        self.assertEqual(r.data, b'{"ipv4": null, "ipv6": true}')

        expected_query_1 = json.dumps([
            '--json', 'dns', 'del', 'example.com', '1']).encode()
        expected_query_2 = json.dumps([
            '--json', 'dns', 'add', 'example.com', 'trurl.example.com', 'AAAA',
            self.alt_ipv6_address, '60']).encode()

        self.assertEqual([call(expected_query_1), call(expected_query_2)],
                         self.devil._send_via_socket.call_args_list)

    def test_update_ipv4_should_not_affect_ipv6(self):
        self.devil.get_hosts = Mock(return_value=[
            {
                "id": 1,
                "record": "trurl.example.com",
                "dns_type": "A",
                "prio": "None",
                "weight": "None",
                "ttl": 60,
                "content": self.valid_request['ipv4'],
                "domain": 1,
            },
            {
                "id": 2,
                "record": "trurl.example.com",
                "dns_type": "AAAA",
                "prio": "None",
                "weight": "None",
                "ttl": 60,
                "content": self.valid_request['ipv6'],
                "domain": 1,
            }], )

        del self.valid_request['ipv6']
        self.valid_request['ipv4'] = self.alt_ipv4_address

        r = self.app.post('/update/', data=self.valid_request)
        self.assertEqual(r.status_code, 200)
        self.assertEqual(r.data, b'{"ipv4": true, "ipv6": null}')

        expected_query_1 = json.dumps([
            '--json', 'dns', 'del', 'example.com', '1']).encode()
        expected_query_2 = json.dumps([
            '--json', 'dns', 'add', 'example.com', 'trurl.example.com', 'A',
            self.alt_ipv4_address, '60']).encode()
        self.assertEqual([call(expected_query_1), call(expected_query_2)],
                         self.devil._send_via_socket.call_args_list)

    def test_update_ipv6_should_not_affect_ipv4(self):
        self.devil.get_hosts = Mock(return_value=[
            {
                "id": 1,
                "record": "trurl.example.com",
                "dns_type": "A",
                "prio": "None",
                "weight": "None",
                "ttl": 60,
                "content": self.valid_request['ipv4'],
                "domain": 1,
            },
            {
                "id": 2,
                "record": "trurl.example.com",
                "dns_type": "AAAA",
                "prio": "None",
                "weight": "None",
                "ttl": 60,
                "content": self.valid_request['ipv6'],
                "domain": 1,
            }], )
        del self.valid_request['ipv4']
        self.valid_request['ipv6'] = self.alt_ipv6_address

        r = self.app.post('/update/', data=self.valid_request)
        self.assertEqual(200, r.status_code)
        self.assertEqual(b'{"ipv4": null, "ipv6": true}', r.data)

        expected_query_1 = json.dumps([
            '--json', 'dns', 'del', 'example.com', '2']).encode()
        expected_query_2 = json.dumps([
            '--json', 'dns', 'add', 'example.com', 'trurl.example.com', 'AAAA',
            self.alt_ipv6_address, '60']).encode()
        self.assertEqual([call(expected_query_1), call(expected_query_2)],
                         self.devil._send_via_socket.call_args_list)

    def test_create_dns_record_detect_ip_address_type_ipv4(self):
        r = self.app.post('/update/', data=self.valid_request_ipv4)
        self.assertEqual(r.status_code, 200)
        self.assertEqual(r.data, b'{"ipv4": true, "ipv6": null}')

    def test_create_dns_record_detect_ip_address_type_ipv6(self):
        r = self.app.post('/update/', data=self.valid_request_ipv6)
        self.assertEqual(r.status_code, 200)
        self.assertEqual(r.data, b'{"ipv4": null, "ipv6": true}')

    def test_prevent_from_mixing_ip_address_types(self):
        self.valid_request['ip'] = '0.0.0.0'
        r = self.app.post('/update/', data=self.valid_request)
        self.assertEqual(r.status_code, 400)
